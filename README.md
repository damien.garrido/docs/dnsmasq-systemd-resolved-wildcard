# Dnsmasq systemd-resolved wildcard
Set a DNS wildcard with Dnsmasq along with systemd-resolved.

## Ubuntu

### Install Dnsmasq
```bash
sudo apt install dnsmasq
```

### Configure Dnsmasq
Let systemd-resolved manage almost every DNS related things except our wildcard domain.

#### Global configuration
Edit `/etc/dnsmasq.conf`.
```bash
sudo vim /etc/dnsmasq.d/wildcard.all.example.com.conf
```

Listen on `127.0.0.1` only.
>bind-interfaces<br/>
>listen-address=127.0.0.1

Do not read `/etc/hosts` neither `/etc/resolv.conf`.
>no-hosts<br/>
>no-resolv

Let Dnsmasq load our configuration file.
>conf-dir=/etc/dnsmasq.d/,*.conf

#### Configure wildcard
Edit a new file under `/etc/dnsmasq.d/`.
```bash
sudo vim /etc/dnsmasq.d/wildcard.all.example.com.conf
```

Let resolve `*.all.example.com` to `192.168.0.10`.
>address=/all.example.com/192.168.0.10

#### Start Dnsmasq
```bash
sudo systemctl start dnsmasq.service
```

### Configure systemd-resolved
Edit `/etc/systemd/resolved.conf`.

Forward requests to Dnsmasq.
>[Resolve]<br/>
>DNS=127.0.0.1

#### Restart systemd-resolved
```bash
sudo systemctl restart systemd-resolved.service
```

## Test
Request systemd-resolved for `test.all.example.com`.
```bash
dig @127.0.0.53 +short test.all.example.com
```
>192.168.0.10
